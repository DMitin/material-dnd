import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import ItemTypes from '../ItemTypes';
import { DragSource, DropTarget } from 'react-dnd';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';


import {List, ListItem} from 'material-ui/List';

import DragItem from './DragItem'

export default class DragList extends Component {
    render() {
        return(
            <List>
                <DragItem />
                <DragItem />
            </List>
        )
    }
}